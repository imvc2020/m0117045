import ddf.minim.*;  //minimライブラリのインポート

Minim minim;  //Minim型変数であるminimの宣言
AudioInput in;  //マイク入力用の変数
int waveH = 100;  //波形の高さ

float xaw = 100;//自分のボールのｘ位置
float yah = 150;//自分のボールのy位置
int balla = 20;//自分のボールの幅 

int bb = 50;//個数
int [] ball = new int[bb];//敵のボール
float [] x = new float[bb];//敵ボールのx位置
float [] y = new float[bb];//敵ボールのy位置
float [] sp = new float[bb];//敵ボールの速さ


int playTime;//ゲーム時間


void settings() {
  size(1000, 500);
}


void setup() {
  minim = new Minim(this);  //初期化
  //バッファ（メモリ上のスペース。この場合は512要素のfloat型の配列）を確保し、マイク入力用の変数inを設定する。
  in = minim.getLineIn(Minim.STEREO, width);
  enemy();
}

void enemy() {//敵ボールの生成
  for (int i=0; i<bb; i++) {
    ball[i] = int(random(10, 30)+0.5);
    while (true) {//ボールのランダム発生の条件
      x[i] = int(random(ball[i]/2, width-ball[i]/2)+0.5);
      y[i] = int(random(ball[i]/2, height-ball[i]/2)+0.5);
      if (dist(x[i], y[i], xaw, yah)>300) break;
    }
    sp[i] = random(1);
  }
}


void draw() {  
  background(0);
  stroke(255);

  float level = in.left.level();//振幅の大きさを拾う関数
  for (int i=0; i<bb; i++) {
    if (x[i] <= - ball[i]/2) x[i] = width + ball[i]/2;//xが0になったら、画面の一番端に戻る
    fill(255);//ランダムボールの色
    ellipse(x[i], y[i], ball[i], ball[i]);//ランダムボールの生成
    x[i]--;//ランダムボールの動き
  }

  if (level > 0.1 && level < 0.25) {//声が出ている時
    fill(0, 0, 255);
    stroke(0, 0, 255);
    yah--; 
    xaw= xaw+0.5;
  } else if (level > 0.25) {//振幅が大きい時
    fill(0, 255, 0);
    yah--;     
    xaw = xaw +1;
  } else {//それ以外
    xaw= xaw -0.5;
    yah++;
  }

  strokeWeight(1);
  ellipse(xaw, yah, balla, balla);//マイボール

  for (int i = 0; i < in.bufferSize()-1; i++) {//視覚化した振幅
    strokeWeight(1);
    stroke(255);
    fill(255);
    point(i, height/2 + in.left.get(i)*waveH);  //左の音声の波形を画面上に描く
    point(i, height/2 + 100 + in.right.get(i)*waveH);  //右　　〃
    println(in.left.get(i)*waveH);
  }
  drawhit();
  drawtime();
}


void drawhit() {//あたり判定
  for (int i = 0; i < bb; i++) {
    float d = dist(x[i], y[i], xaw, yah);//点と点を求める関数
    if (d - ball[i]/2 - balla/2 +1 < 0) {//自キャラの半径と敵の最小半径の合計が20だから
      gameOver();
    }
    if (xaw == -balla || yah == height + balla || xaw == width + balla || yah == -balla) {//壁とのあたり判定
      gameOver();
    }
  }
}

void gameOver() {//ゲームオーバーのときの処理
  fill(255, 0, 0);
  ellipse(xaw, yah, balla, balla);//マイボール
  // Game Over 画面出力
  fill(255);
  rect(width/3, height/3 +50, 300, 100);
  fill(204, 0, 0);
  textSize(35);
  text("Game Over", width/3, height/2 );
  fill(255, 100, 00);
  text("Your Score →" + (playTime), width/3, height/2 + 30);
  fill(255, 204, 0);
  text("continue?", width/3, height/2 +60);
  // ループ終了
  noLoop();
}

void drawtime() {//プレイ時間
  textSize(30);
  fill(255, 255, 0);
  text("Time=" + playTime, 20, 30);
  playTime ++;
}


void stop()
{
  in.close();
  minim.stop();

  super.stop();
}

void mousePressed() {
  if (mouseButton !=  LEFT) return;//コンテニュー操作
  enemy();
  playTime = 0;
  xaw = 100;
  yah = 100;
  ellipse(xaw, yah, balla, balla);//マイボール
  loop();
}
